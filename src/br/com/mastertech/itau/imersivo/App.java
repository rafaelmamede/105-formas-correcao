package br.com.mastertech.itau.imersivo;

import java.util.List;

import br.com.mastertech.itau.imersivo.console.Console;
import br.com.mastertech.itau.imersivo.construtores.ConstrutorForma;
import br.com.mastertech.itau.imersivo.objetos.Forma;

public class App {

	public static void main(String[] args) {
		List<Double> lados = Console.lerLados();
		
		Forma forma = ConstrutorForma.construir(lados);
		
		Console.imprimeArea(forma);
	}

}
