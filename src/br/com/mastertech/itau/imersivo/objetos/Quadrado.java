package br.com.mastertech.itau.imersivo.objetos;

public class Quadrado extends Forma{
	
	private double ladoA;
	private double ladoB;
	
	public Quadrado(double ladoA, double ladoB) {
		this.ladoA = ladoA;
		this.ladoB = ladoB;
	}
	
	@Override
	public String getNome() {
		return "Quadrado";
	}
	
	@Override
	public double calculaArea() {
		return ladoA * ladoB;
	}

}
